%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \huge \bf \color{red} Laboratory V  }\\[2mm]
{  \Large \textbf{\textit{\color{red} Forces on a Body in Static Equilibrium}}  }\\[2mm]
{  \large \it \color{azure(colorwheel)} Introductory Physics I --- {\color{darkpink} Physics 171L}  }
\end{center}
\hrule
\bigskip
\begin{tabularx}{\textwidth}{lXr}
%
	&
	{\bf Name:}
	&
	{\bf Laboratory Section:}
	\hspace{1.0cm}
	\\[2mm]
%
	&
	{\bf Partners:}\hspace{5.0cm}
\end{tabularx}
\medskip


%\def\SHOWSOLUTIONS{1}
%\newcommand\solution[1]{{\ifdefined\SHOWSOLUTIONS\color{richelectricblue}#1\fi}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            I N T R O D U C T I O N                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Introduction}}

	The goal of this laboratory is to demonstrate that the sum of the forces acting upon
	an object in static equilibrium (not moving) is zero


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               P R E L I M I N A R Y   I N V E S T I G A T I O N              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textit{Preliminary investigation}}

	Suspend a mass by a string from a spring scale.	
	Note the reading on the scale.
	Now put the string over a pulley so the mass is suspended vertically
	and the spring scale is horizontal.
	Does this change the reading on the spring scale?
	As long as the mass is suspended vertically,
	does the position of the spring scale have any effect on tension in the string
	(the scale reading)?


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                        E X P E R I M E N T   S E T U P                       %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Experiment setup}}

	First the force table must be levelled, and the three risers (with pulleys)
	firmly attached to the rim of the force table with each riser pushed as far onto the table
	as depth of the notch will permit.
	At least one riser must be of a different height than the other two.
	Place one riser at an even degree marking (such as \cc{ \ang{360} })
	to use as the reference for your coordinate system.
	Any unequal riser spacing that does not leave more than \cc{ \ang{160} }
	between adjacent pulleys will do

\bigskip\noindent
	All this adjustment is not without reason or reward.
	Knowing that the table is level assures us that the gravitational force on the bob
	is perpendicular to the plane of the table.
	Knowing the plumb bob is centered and risers are properly attached makes the protractor
	reading meaningful

\begin{figure}[h!]
\begin{center}
\includegraphics[width=6cm, trim={0cm 3cm 0cm 1cm}, clip]{plumb-bob.png}
\vskip -1.0cm
\end{center}
\caption{Plumb bob}
\end{figure}

\bigskip\noindent
	Attach three strings to the plumb bob by removing its top button, passing the strings
	through the hole in the button and knotting them.
	Re--attach the button and pass the strings over the pulleys and attach these strings
	to the slotted mass holders.
	When sufficient slotted masses are placed on the holders, the plumb bob will be
	suspended over the table.
	Since it is not easy to add weights to all three holders simultaneously,
	it is prudent to hold the plumb bob until enough mass is on the three holders
	to suspend the bob near the center of the table

\begin{figure}[h!]
\begin{center}
\begin{tikzpicture}
	% force table
	\draw [draw = midnightblue, thick]
		(0, 0) circle [radius = 5cm];

	% small ticks
	\foreach \j in {0, ..., 359} {
		\draw [draw = midnightblue, thin]
			(\j - 90: 5.0) -- (\j - 90: 4.6);
	}

	% five-degree ticks
	\foreach \j in {0, 5, ..., 355} {
		\draw [draw = midnightblue, thin]
			(\j - 90: 5.0) -- (\j - 90: 4.4);
	}

	% ten-degree ticks
	\foreach \j in {0, 10, ..., 350} {
		\draw [draw = midnightblue, thick]
			(\j - 90: 5.0) -- (\j - 90: 4.2)
			node [pos = 1.0, above, rotate = \j] { \color{midnightblue} \tiny \textsf{\j} };
	}

	% inner circle
	\foreach \j in {0, 2, ..., 358} {
		\draw [draw = midnightblue, thin]
			(\j - 90: 1.6) -- (\j - 90: 1.9);
	}

	\foreach \j in {0, 10, ..., 350} {
		\draw [draw = midnightblue, thick]
			(\j - 90: 1.6) -- (\j - 90: 2.1);
	}

	\foreach \j in {0, 20, ..., 340} {
		\node at (\j - 90: 2.1) [below, rotate = \j] { \fontsize{5}{6} \color{midnightblue} \!\!\textsf{\j} };
	}

	% big thick circle
	\draw [draw = midnightblue, line width = 3.6mm]
		(0, 0) circle [radius = 0.7cm];

	% hole in the middle
	\draw [draw = midnightblue, ultra thin, fill = midnightblue!50]
		(0, 0) circle [radius = 0.2cm];
\end{tikzpicture}
\end{center}
\caption{Force table}
\end{figure}

\bigskip\noindent
	The final adjustment is made by adding or removing masses to center the bob over the table.
	The centering is checked by sighting both the plumb bob and the table center pin
	using first one and then another of the risers as vertical reference.
	Once the bob is centered, determine the range of masses that will balance the combined forces
	by making \emph{small} adjustments to the mass suspended from each pulley,
	one at a time.
	Tapping gently on the table improves the sensitivity of the apparatus.
	The forces are balanced when the bob is centered and does not move when you tap the table.
	What is the biggest change you can make to the mass on each string before the balance is lost?
	What does it represent?
\begin{itemize}
\item
	Make a plan (top) view and elevation (side) view diagram of the force table
	\emph{as you have it configured}
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   U N D E R S T A N D I N G   V E C T O R S                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Understanding vectors}}

	There are four obvious forces acting on the plumb bob:
	gravity and the tensions in each of the three strings.
	If we assume that forces are vector quantities, we must find not only the magnitude of each force
	but its direction as well.
	The magnitude of the forces is easily found by noting all the masses involved.
	The masses of the plumb bob, the slotted masses, and the mass holders are all easily found,
	but specifying the directions of the forces requires some care.
	To facilitate discussion, let us agree to call the horizontal plane of the table
	the \cc{ xy }--plane and to call the vertical line up from the center pin
	(perpendicular to the table) the \cc{ +z } direction.
	The gravitational force on the bob is obviously exerted entirely in the \cc{ -z } direction
	while each of the other three forces act in a unique direction that can be specified by measuring the direction
	of its projection on the \cc{ xy }--plane and the angle \cc{ \theta } that the force makes with the \cc{ z }--axis.
	The former is found by the (horizontal) angle each riser forms around the edge of the table
	with respect to your reference riser and the latter can be found by placing the vertical grid plate behind
	each string and noting how the string crosses the grid lines

\bigskip\noindent
	Thus, \cc{ \tan\,\theta } and \cc{ \theta } itself can be found for each force by measuring
	\cc{ \Delta z } and \cc{ \Delta h } for each string as it crosses the ruled plate

\bigskip
\begin{minipage}[b][8.4cm][t]{0.54\textwidth}
\begin{center}
\begin{tikzpicture}
	% part of plumb bob length
	\begin{scope}[shift={(0, -0.82)}]
		\draw [draw = none, fill = silver!60]
			(-0.4, -0.2) -- (0.4, -0.2) -- (0.8, 0) -- (0.4, 0.2) -- (-0.4, 0.2) -- (-0.8, 0) -- cycle;
		\draw [draw = none, fill = platinum]
			(-0.8, 0) -- ++(0, -2) -- ++(0.4, -0.2) -- ++(0, 2) -- cycle
			(0.8, 0) -- ++(0, -2) -- ++(-0.4, -0.2) -- ++(0, 2) -- cycle;
		\draw [draw = none, fill = platinum!30]
			(-0.4, -0.2) -- (0.4, -0.2) -- ++(0, -2) -- ++(-0.8, 0) -- cycle;
        \end{scope}

	% top of the plumb bob
	\begin{scope}[shift={(0, -0.7)}]		% disc adjacent to the plumb bob's body
		\draw [draw = none, fill = silver!50]
			(-0.5, 0) arc [x radius = 0.5, y radius = 0.1, start angle = -170, end angle = 360 - 170];
		\draw [draw = none, left color = darkgray, right color = darkgray, middle color = darkgray!10]
			(-0.5, 0) arc [x radius = 0.5, y radius = 0.1, start angle = -170, end angle = -10]
			-- ++(0, -0.15)
			arc [x radius = 0.5, y radius = 0.1, start angle = -10, end angle = -170]
			-- cycle;
        \end{scope}
	\draw [draw = none, left color = darkgray, right color = darkgray, middle color = silver!10]
		(-0.25, -0.3) arc [x radius = 0.25, y radius = 0.05, start angle = -170, end angle = -10]
		-- ++(0, -0.4)
		arc [x radius = 0.25, y radius = 0.05, start angle = -10, end angle = -170]
		-- cycle;
	\draw [draw = none, fill = silver!50]
		(-0.5, 0) arc [x radius = 0.5, y radius = 0.1, start angle = -170, end angle = 360 - 170];
	\draw [draw = none, left color = silver, right color = silver, middle color = silver!10]
		(-0.5, 0) arc [x radius = 0.5, y radius = 0.1, start angle = -170, end angle = -10]
		-- ++(0, -0.3)
		arc [x radius = 0.5, y radius = 0.1, start angle = -10, end angle = -170]
		-- cycle;

	% vertical axis
	\draw [draw = ProcessBlue, dash pattern = on 2mm off 0.8mm on 0.2mm off 0.8mm]
		(0, 0) -- ++(0, 5);

	% grid
	\tikzmath{
		\stringangle = atan(5. / 5.);
	}
	\begin{scope}[shift={(2.4, 2.4)}]
		\draw [draw = midnightblue, thick, step = 0.4cm]
			(0, 0) grid (2.4, 2.4);
		\draw [draw = midnightblue, line width = 0.6mm, cap = round]
			(0, 0) -- ++(0, 2.4)
			node [midway, above, rotate = 90, yshift = 1mm] {\cc{ \Delta z }}
			 -- ++(2.4, 0)
			node [midway, above, yshift = 1mm] {\cc{ \Delta h }};
		\draw [line width = 3mm, shorten <= 0.3mm, draw = white, draw opacity = 0.9]	% shade the background
			(90: 1.0) arc [radius = 1.0, start angle = 90, end angle = \stringangle];
		\draw [draw = ProcessBlue, <->, >={Stealth[length=1.4mm]}, shorten >=0.3mm, shorten <= 0.3mm]
			(90: 1.0) arc [radius = 1.0, start angle = 90, end angle = \stringangle]
			node [midway, above, xshift = 1mm, circle, fill = white, fill opacity = 0.9] {\cc{ \theta }};
	\end{scope}

	% strings
	\draw [line width = 0.6mm, draw = persianplum, cap = round]
		(0, 0) -- (-2, 1.5)
		(0, 0) -- (-1.6, 2.5)
		(0, 0) -- (\stringangle: 7.0);
	\draw [draw = ProcessBlue, <->, >={Stealth[length=1.4mm]}, shorten >=0.3mm]
		(90: 1.0) arc [radius = 1.0, start angle = 90, end angle = \stringangle]
		node [midway, above, xshift = 1mm] {\cc{ \theta }};
\end{tikzpicture}
\end{center}
\end{minipage}
\begin{minipage}[b][8.4cm][t]{0.44\textwidth}
\ccc
\begin{align*}
%
	&
\hskip -1.6cm
	\tan\,\theta	~~=~~	\frac{\Delta h}{\Delta z}
	\\[2mm]
%
	&
\hskip -1.6cm
	\theta		~~=~~	\arctan\, \big(\, \Delta h \,/\, \Delta z \,\big)
\end{align*}
\end{minipage}

\bigskip\noindent
	We now have sufficient information to make a summation of the forces acting on the plumb bob.
	A geometrical analysis is preferred by some students, and, although a trigonometric approach
	is presented here, a sketch of the location and approximate magnitude of force projections
	in the \cc{ xy }--plane will prove of great value in avoiding confusion

\bigskip\noindent
	We are assuming that forces are vector quantities and as such they may be resolved into components

\bigskip
\begin{minipage}[b][7.0cm][t]{0.6\textwidth}
\begin{center}
\begin{tikzpicture}
	% coordinate axes
	\draw [draw = ProcessBlue, thin]
		(0, 6) |- (6, 0);

	% vectors
	\draw [draw = internationalorange, line width = 0.8mm, ->, > = {Stealth[length=4mm]}]
		(0, 0) -- (45: 5.4)
		node [midway, above, shift = {(-1mm, 1mm)}] { \cc{ \vec{S} } };
	\draw [draw = ProcessBlue, very thin, shorten >= 0.4mm, <->, >={Stealth[length=1.4mm]}]
		(0, 1.0) arc [radius = 1.0cm, start angle = 90, end angle = 45]
		node [midway, above, xshift = 1mm] { \cc{ \theta } };
	\draw [draw = internationalorange, very thick, dash pattern = on 1mm off 1mm]
		(45: 5.4) -- ({5.4 * cos(45)}, 0)
		node [midway, right] { \cc{ \vec{S}{}_z } };
	\draw [draw = internationalorange, very thick, dash pattern = on 1mm off 1mm]
		(45: 5.4) -- (0, {5.4 * sin(45)})
		node [midway, above] { \cc{ \vec{S}{}_{xy} } };
\end{tikzpicture}
\end{center}
\end{minipage}
\begin{minipage}[b][7.0cm][t]{0.35\textwidth}
\ccc
\begin{align*}
%
	&
\hskip -1.6cm
	\big| \vec S{}_z \big|		~~=~~		\big| \vec S \big|\, \cos\,\theta
	\\[2mm]
%
	&
\hskip -1.6cm
	\big| \vec S{}_{xy} \big|	~~=~~		\big| \vec S \big|\, \sin\,\theta
\end{align*}
\end{minipage}

\noindent
	In the diagram above, tension force on the string \cc{ \vec S } has been resolved into components
	\cc{ \vec S{}_z } (vertical) and \cc{ \vec S{}_{xy} } (horizontal),
	\emph{i.e.} \cc{ \vec S } can be exactly replaced by the combination \cc{ \vec S{}_z ~+~ \vec S{}_{xy} }.
	Each of the three tension forces can be resolved in this way yielding \cc{ z }--components
	and components (projections) in the \cc{ xy }--plane

\bigskip\noindent
	As mentioned before, the vector diagram in the \cc{ xy }--plane deserves at least a sketch to help keep it straight.
	In this diagram, each string has been designated with a letter, A, B and C

\bigskip
\begin{minipage}[b][9.4cm][t]{0.6\textwidth}
\begin{center}
\begin{tikzpicture}
	% coordinate axes
	\draw [draw = ProcessBlue, thin]
		(-4, 0) -- (4, 0)
		node [pos = 1, xshift = 4mm] { \cc{ \ang{0} } }
		node [pos = 0, xshift = -6mm] { \cc{ \ang{180} } };
	\draw [draw = ProcessBlue, thin]
		(0, -4) -- (0, 4)
		node [pos = 1, yshift = 3mm, xshift = 1mm] { \cc{ \ang{90} } }
		node [pos = 0, yshift = -4mm] { \cc{ \ang{270} } };

	% vectors
	\draw [draw = internationalorange, line width = 0.8mm, ->, > = {Stealth[length=4mm]}]
		(0, 0) -- (45: 4.4)
		node [midway, above, shift = {(-2mm, 1mm)}] { \cc{ \vec A{}_{xy} } };
	\draw [draw = ProcessBlue, very thin, shorten >= 0.4mm, <->, >={Stealth[length=1.4mm]}]
		(1.0, 0) arc [radius = 1.0cm, start angle = 0, end angle = 45]
		node [midway, right, yshift = 1mm] { \cc{ \phi } };
	\draw [draw = internationalorange, very thick, dash pattern = on 1mm off 1mm]
		(45: 4.4) -- ({4.4 * cos(45)}, 0)
		node [midway, right] { \cc{ \vec{A}{}_y } };
	\draw [draw = internationalorange, very thick, dash pattern = on 1mm off 1mm]
		(45: 4.4) -- (0, {4.4 * sin(45)})
		node [midway, above] { \cc{ \vec{A}{}_x } };

	\draw [draw = internationalorange, line width = 0.8mm, ->, > = {Stealth[length=4mm]}]
		(0, 0) -- (155: 3.8)
		node [midway, above, shift = {(1mm, 1mm)}] { \cc{ \vec B{}_{xy} } };

	\draw [draw = internationalorange, line width = 0.8mm, ->, > = {Stealth[length=4mm]}]
		(0, 0) -- (-112: 3.0)
		node [midway, above, shift = {(-5mm, 0)}] { \cc{ \vec C{}_{xy} } };
\end{tikzpicture}
\end{center}
\end{minipage}
\begin{minipage}[b][9.4cm][t]{0.35\textwidth}
\ccc
\begin{align*}
%
	&
	\vec A{}_{xy}			~~=~~	\vec A{}_x ~+~ \vec A{}_y
	\\[2mm]
%
	&
	\big| \vec A{}_x \big|	~~=~~		\big| \vec A{}_{xy} \big|\, \cos\,\phi
	\\[2mm]
%
	&
	\big| \vec A{}_y \big|	~~=~~		\big| \vec A{}_{xy} \big|\, \sin\,\phi
\end{align*}
\end{minipage}

\bigskip\noindent
	The horizontal (\cc{ xy }) components of the three tension forces are shown in the sketch above,
	with the further resolution of \cc{ \vec A{}_{xy} } into \cc{ \vec A{}_x } and \cc{ \vec A{}_y }.
	Each of the three horizontal (\cc{ xy }) components of the string tensions
	\cc{ \vec A }, \cc{ \vec B } and \cc{ \vec C } can be thus resolved into \cc{ x } and \cc{ y }
	components

\bigskip\noindent
	Finally we can find the algebraic sum of all \cc{ x }--components, of all \cc{ y }--components
	and of all \cc{ z }--components --- of the four forces acting on the plumb bob ---
	and perhaps draw some conclusions about the sum of all forces on a body in equilibrium
	and about the validity of our assumption that forces are vector quantities

\bigskip\noindent
	Of course one trial does not constitute a conclusive experiment.
	Perhaps we chanced upon a unique configuration where the numbers are misleading?
	The same way one might conclude that multiplication and addition are identical
	if one considered only \cc{ 2 \,\times\, 2 } and \cc{ 2 \,+\, 2 }.
	Therefore, we should change the riser positions and repeat the experiment before
	drawing conclusions

\begin{itemize}
\item
	Sum the \cc{ x }, \cc{ y } and \cc{ z } components of each of the four forces algebraically.
	Do they add up to zero?
	If not, what does the resultant vector represent?
	Do the forces exactly cancel one another out in reality?
	How can you tell?

\item
	Show the addition of the vertical (\cc{ z }) components of all forces acting on the plumb bob graphically.
	Show graphically the addition of the horizontal (\cc{ xy }) components of the forces acting on the plumb bob.
	Do the resulting vectors match the vector resulting from you algebraic sum of the forces?
	Why or why not?
\end{itemize}


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   L A B O R A T O R Y   E V A L U A T I O N                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Laboratory evaluation}}

	Please provide feedback on the following areas, comparing this laboratory to your previous labs.
	Please assign each of the listed categories a value in 1 ~--~ 5, with 5 being the best, 1 the worst.

\bigskip
\begin{itemize}
\item
	how much fun you had completing this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well the lab preparation period explained this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the amount of work required compared to the time allotted?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	your understanding of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the difficulty of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well this laboratory tied in with the lecture?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5
\end{itemize}

\bigskip\noindent
	Comments supporting or elaborating on your assessment can also be very helpful in improving the future laboratories


\end{document}
