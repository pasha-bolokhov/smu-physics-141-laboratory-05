#include "shapes.inc"
#include "colors.inc"
#include "metals.inc"
#include "textures.inc"


#declare plumb_bob = union {

	// the length of the plumb bob
	prism {
		linear_spline
		-1.6, 1.6, 7
		<vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 0).x, vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 0).z>,
		<vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 60).x, vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 60).z>,
		<vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 120).x, vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 120).z>,
		<vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 180).x, vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 180).z>,
		<vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 240).x, vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 240).z>,
		<vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 300).x, vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 300).z>,
		<vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 0).x, vaxis_rotate(<1, 0, 0>, <0, 1, 0>, 0).z>
	}
	
	// bottom of the plumb bob
	union {
		difference {
			union {
				cylinder {
					<0, -1.6 - 0.7 - 0.22, 0>, <0, -1.6 - 0.7, 0>, 0.7
				}
				difference {
					cylinder {
						<0, -1.6 - 0.7, 0>, <0, -1.6 - 0.16, 0>, 0.4
					}
					cylinder {
						<0, -1, 0>, <0, 1, 0>, 0.2
						rotate <90, 0, 0>
						translate <0, -1.6 - (0.7 + 0.16) / 2.0, 0>
					}
				}
			}
			cylinder {
				<0, -1.6 - 1, 0>, <0, -1.6 - 0.6, 0>, 0.2
			}
		}
		cylinder {
			<0, -1.6 - 0.16, 0>, <0, -1.6 - 0.0, 0>, 0.7
		}
		texture {
			finish {
				ambient .1
				diffuse .4
				reflection .25
				specular 0.9
				roughness 0.1
				metallic
			}	
			pigment { color Silver }
			normal { bumps 0.2 scale 0.02 }
			finish { phong 0.5 }
		}
	}
	
	difference {
		cone {
			<0, 3.0, 0>, 0
			<0, 1.6, 0>, 1.0
		}
		union {
			// form the faces
			box {
				<-8, 0, -cos(pi/6.0)>, <8, 8, -5>
			}
			box {
				<-8, 0, -cos(pi/6.0)>, <8, 8, -5>
				rotate <0, 60, 0>
			}
			box {
				<-8, 0, -cos(pi/6.0)>, <8, 8, -5>
				rotate <0, 120, 0>
			}
			box {
				<-8, 0, -cos(pi/6.0)>, <8, 8, -5>
				rotate <0, 180, 0>
			}
			box {
				<-8, 0, -cos(pi/6.0)>, <8, 8, -5>
				rotate <0, 240, 0>
			}
			box {
				<-8, 0, -cos(pi/6.0)>, <8, 8, -5>
				rotate <0, 300, 0>
			}
	
			// cut it to below
			box {
				<-8, 1.6, 8>, <8, -5, -8>
			}
		}
	}
	
	texture {
		finish {
			ambient .1
			diffuse .4
			reflection .25
			specular 0.9
			roughness 0.1
			metallic
		}	
		pigment { color Silver }
		normal { bumps 0.2 scale 0.02 }
		finish { phong 0.5 }
	}
}

object {
	plumb_bob
	rotate -90 * z
}

camera {
	location <-6.0, 0, -16>
	angle 20.0
	look_at <0, 0, 0>
}

light_source {
	<0, 0, -100>
	color White
}
