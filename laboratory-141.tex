%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \huge \bf \color{red} Laboratory V  }\\[2mm]
{  \Large \textbf{\textit{\color{red} Forces on a Body in Static Equilibrium}}  }\\[2mm]
{  \large \it \color{azure(colorwheel)} Introductory Physics I --- {\color{darkorange} Physics 141L}  }
\end{center}
\hrule
\bigskip
\begin{tabularx}{\textwidth}{lXr}
%
	&
	{\bf Name:}
	&
	{\bf Laboratory Section:}
	\hspace{1.0cm}
	\\[2mm]
%
	&
	{\bf Partners:}\hspace{5.0cm}
\end{tabularx}
\medskip


%\def\SHOWSOLUTIONS{1}
%\newcommand\solution[1]{{\ifdefined\SHOWSOLUTIONS\color{richelectricblue}#1\fi}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            I N T R O D U C T I O N                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Introduction}}

	The goal of this laboratory is to demonstrate that the sum of the forces acting upon
	an object in static equilibrium (not moving) is zero


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               P R E L I M I N A R Y   I N V E S T I G A T I O N              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textit{Preliminary investigation}}

	Suspend a mass by a string from a spring scale.
	Note the reading on a scale.
	Now put the string over a pulley so that the mass is suspended vertically
	and the spring scale is horizontal.
	Does this change the reading on the spring scale?
	As long as the mass is suspended vertically, does the position of the spring scale have any effect
	on tension in the string (the scale reading)?


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                        E X P E R I M E N T   S E T U P                       %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Experiment setup}}

	Ensure the force table is level and attach three pulleys firmly to the rim.
	Place one pulley at an even degree marking (such as \cc{ \ang{360} }) to use as the reference
	for your coordinate system.
	Any pulley spacing other than even \cc{ \ang{120} } spacing that does not leave more than
	\cc{ \ang{160} } between adjacent pulleys will do

\begin{figure}[h!]
\begin{center}
\begin{tikzpicture}
	% force table
	\draw [draw = midnightblue, thick]
		(0, 0) circle [radius = 5cm];

	% small ticks
	\foreach \j in {0, ..., 359} {
		\draw [draw = midnightblue, thin]
			(\j - 90: 5.0) -- (\j - 90: 4.6);
	}

	% five-degree ticks
	\foreach \j in {0, 5, ..., 355} {
		\draw [draw = midnightblue, thin]
			(\j - 90: 5.0) -- (\j - 90: 4.4);
	}

	% ten-degree ticks
	\foreach \j in {0, 10, ..., 350} {
		\draw [draw = midnightblue, thick]
			(\j - 90: 5.0) -- (\j - 90: 4.2)
			node [pos = 1.0, above, rotate = \j] { \color{midnightblue} \tiny \textsf{\j} };
	}

	% inner circle
	\foreach \j in {0, 2, ..., 358} {
		\draw [draw = midnightblue, thin]
			(\j - 90: 1.6) -- (\j - 90: 1.9);
	}

	\foreach \j in {0, 10, ..., 350} {
		\draw [draw = midnightblue, thick]
			(\j - 90: 1.6) -- (\j - 90: 2.1);
	}

	\foreach \j in {0, 20, ..., 340} {
		\node at (\j - 90: 2.1) [below, rotate = \j] { \fontsize{5}{6} \color{midnightblue} \!\!\textsf{\j} };
	}

	% big thick circle
	\draw [draw = midnightblue, line width = 3.6mm]
		(0, 0) circle [radius = 0.7cm];

	% hole in the middle
	\draw [draw = midnightblue, ultra thin, fill = midnightblue!50]
		(0, 0) circle [radius = 0.2cm];
\end{tikzpicture}
\end{center}
\caption{Force table}
\end{figure}

\bigskip\noindent
	Attach three strings to a metal ring, loosely enough that the string can slide around the ring.
	Pass the strings over the three pulleys and attach a mass holder to each one.
	Place sufficient slotted masses on the mass holders to suspend the ring over the center of the table.
	Since it is not easy to add mass to all three holders simultaneously, it is prudent to hold the ring until
	enough weight is on the three holders to suspend it near the center of the table

\bigskip\noindent
	The final adjustment is made by adding or removing mass to center the ring over the table.
	The centering is checked by sighting from as close to directly above the table as is practical,
	as well as from various side angles.
	Once the ring is centered, determine the range of masses that will balance the combined forces by making
	\emph{small} adjustments to the mass suspended from each pulley, one at a time.
	Tapping gently on the table improves the sensitivity of the apparatus.
	The forces are balanced when the ring is centered and does not move when you tap the table.
	What is the biggest change you can make to the mass before the balance is lost?
	What does this represent?
\begin{itemize}
\item
	Make a plan (top) view diagram of the force table \emph{as you have it configured}
\end{itemize}
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   U N D E R S T A N D I N G   V E C T O R S                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Understanding vectors}}

	There are three forces acting on the ring: the tensions in each of the three strings.
	If we assume that forces are vector quantities, we must find not only the magnitude
	of each force but its direction as well.
	The magnitudes of the forces are easily found by noting all the masses involved.
	The total mass of the slotted masses and holder on each string is easily found,
	but specifying the directions of the forces requires some care.
	To facilitate discussion, let us agree to call the horizontal plane of the table
	the \cc{ xy }--plane.
	Each of the three forces acts in a unique direction that can be specified by measuring
	the direction of its projection on the \cc{ xy }--plane with respect to the reference pulley

\bigskip\noindent
	We now have sufficient information to make a summation of the forces acting on the ring.
	A geometrical analysis is preferred by some, and, although a trigonometric approach is presented here,
	a sketch of the location and approximate magnitude of force projections in the \cc{ xy}--plane
	will prove of great value in avoiding confusion

\bigskip\noindent
	We are assuming that forces are vector quantities and as such they may be resolved into components.
	The following vector diagram in the \cc{ xy }--plane illustrates the determination
	of the \cc{ x } and \cc{ y } components of the tension forces.
	Notice that each string has been given a letter designation to help keep the data organized

\begin{minipage}[b][9.4cm][t]{0.6\textwidth}
\begin{center}
\begin{tikzpicture}
	% coordinate axes
	\draw [draw = ProcessBlue, thin]
		(-4, 0) -- (4, 0)
		node [pos = 1, xshift = 4mm] { \cc{ \ang{0} } }
		node [pos = 0, xshift = -6mm] { \cc{ \ang{180} } };
	\draw [draw = ProcessBlue, thin]
		(0, -4) -- (0, 4)
		node [pos = 1, yshift = 3mm, xshift = 1mm] { \cc{ \ang{90} } }
		node [pos = 0, yshift = -4mm] { \cc{ \ang{270} } };

	% vectors
	\draw [draw = internationalorange, line width = 0.8mm, ->, > = {Stealth[length=4mm]}]
		(0, 0) -- (45: 4.4)
		node [midway, above, shift = {(-1mm, 1mm)}] { \cc{ \vec{A} } };
	\draw [draw = ProcessBlue, very thin, shorten >= 0.4mm, <->, >={Stealth[length=1.4mm]}]
		(1.0, 0) arc [radius = 1.0cm, start angle = 0, end angle = 45]
		node [midway, right, yshift = 1mm] { \cc{ \phi } };
	\draw [draw = internationalorange, very thick, dash pattern = on 1mm off 1mm]
		(45: 4.4) -- ({4.4 * cos(45)}, 0)
		node [midway, right] { \cc{ \vec{A}{}_y } };
	\draw [draw = internationalorange, very thick, dash pattern = on 1mm off 1mm]
		(45: 4.4) -- (0, {4.4 * sin(45)})
		node [midway, above] { \cc{ \vec{A}{}_x } };

	\draw [draw = internationalorange, line width = 0.8mm, ->, > = {Stealth[length=4mm]}]
		(0, 0) -- (155: 3.8)
		node [midway, above, shift = {(1mm, 1mm)}] { \cc{ \vec{B} } };

	\draw [draw = internationalorange, line width = 0.8mm, ->, > = {Stealth[length=4mm]}]
		(0, 0) -- (-112: 3.0)
		node [midway, above, shift = {(-3mm, 0)}] { \cc{ \vec C } };
\end{tikzpicture}
\end{center}
\end{minipage}
\begin{minipage}[b][9.4cm][t]{0.35\textwidth}
\ccc
\begin{align*}
%
	&
	\vec{A}			~~=~~	\vec A{}_x ~+~ \vec A{}_y
	\\[2mm]
%
	&
	\big| \vec A{}_x \big|	~~=~~		\big| \vec A \big|\, \cos\,\phi
	\\[2mm]
%
	&
	\big| \vec A{}_y \big|	~~=~~		\big| \vec A \big|\, \sin\,\phi
\end{align*}
\end{minipage}

\noindent
	The three tension forces are shown in the sketch above, with the resolution of
	\cc{ \vec A } into \cc{ \vec A{}_x } and \cc{ \vec A{}_y }.
	Each of the three tension forces can be thus resolved into \cc{ x } and \cc{ y } components


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                             T A K I N G   D A T A                            %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Taking data}
	We can find the algebraic sum of all \cc{ x } components and of all \cc{ y } components
	and perhaps draw some conclusions about the sum of all forces on a body in equilibrium
	and about the validity of our assumption that forces are vector quantities

\bigskip\noindent
	Of course one trial does not constitute a conclusive experiment.
	Perhaps we chanced upon a unique configuration where the numbers are misleading?
	The same way one might conclude that multiplication and addition are identical
	if one considered only \cc{ 2 \,\times\, 2 } and \cc{ 2 \,+\, 2 }.
	Therefore, we could change the weights and the pulley positions,
	and repeat the experiment before drawing conclusions

\begin{itemize}
\item
	Sum the \cc{ x } and \cc{ y } components of each of the forces algebraically.
	Do hey add up to zero?
	If not, what does the resultant vector represent?
	Do the forces exactly cancel one another out in reality?
	How can you tell?

\item
	Show graphically the addition of the forces acting on the ring.
	Does the resulting vector match the vector resulting from your algebraic sum of the forces?
	Why or why not?
\end{itemize}


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   L A B O R A T O R Y   E V A L U A T I O N                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Laboratory evaluation}}

	Please provide feedback on the following areas, comparing this laboratory to your previous labs.
	Please assign each of the listed categories a value in 1 ~--~ 5, with 5 being the best, 1 the worst.

\bigskip
\begin{itemize}
\item
	how much fun you had completing this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well the lab preparation period explained this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the amount of work required compared to the time allotted?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	your understanding of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the difficulty of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well this laboratory tied in with the lecture?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5
\end{itemize}

\bigskip\noindent
	Comments supporting or elaborating on your assessment can also be very helpful in improving the future laboratories


\end{document}
